<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class EcovacController extends Controller
{
    public function config(Request $request)
    {
        $real = $this->forward($request);dd($request, $real);
    }

    // xu ly cac request co path la /api/appsvr/app.do
    public function handle(Request $request)
    {
        $real = $this->forward($request);
        $headers = collect($real->headers())->map(function ($header) {
            return $header[0];
        })->toArray();

        $json = $real->json();
        if ($real->ok() && $json) {
            $json['devices'] = collect($json['devices'])->map(function ($device) {
                $device['did'] = Str::replace('black_list_', '', $device['did']);
                return $device;
            })->toArray();

            return response()->json($json, $real->status(), $headers);
        }

        return response($real->body(), $real->status(), $headers);
    }

    // xu ly cac request khac
    public function fallback(Request $request)
    {
        $real = $this->forward($request);
        $json = $real->json();
        $headers = collect($real->headers())->map(function ($header) {
            return $header[0];
        })->toArray();

        if ($real->ok() && $json) {
            return response()->json($json, $real->status(), $headers);
        }

        return response($real->body(), $real->status(), $headers);
    }

    // chuyen request sang server ecovac
    protected function forward(Request $request)
    {
        $body = $request->input();
        $headers = collect($request->headers->all())->map(function ($header) {
            return $header[0];
        })->toArray();

        $url = $request->fullUrl();
        // $url = Str::replace('https://api-app.dc-cn.cn.ecouser.net/', 'https://47.114.148.125/', $url);

        if ($request->isMethod('get')) {
            $response = Http::withHeaders($headers)->connectTimeout(30)->get($url);
        } else {
            $response = Http::withHeaders($headers)->connectTimeout(30)->post($url, $body);
        }

        /*Log::info('Forward request', [
            'url' => $request->fullUrl(),
            'method' => $request->method(),
            'body' => $body,
            'headers' => $headers,
            'raw' => $response->body(),
            'response' => $response->json(),
        ]);*/
        return $response;
    }
}
