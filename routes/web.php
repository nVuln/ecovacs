<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::any('/api/appsvr/app/config', [\App\Http\Controllers\EcovacController::class, 'config']);
Route::any('/api/appsvr/app.do', [\App\Http\Controllers\EcovacController::class, 'handle']);
Route::any('{any}', [\App\Http\Controllers\EcovacController::class, 'fallback'])->where('any', '.*');
